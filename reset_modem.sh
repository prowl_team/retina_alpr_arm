#!/bin/bash
# */20 * * * *  /home/pi/retina_alpr_arm/reset_modem.sh

 function check_connectivity() {

    local test_ip
    local test_count

    test_ip="8.8.8.8"
    test_count=1

    if ping -c ${test_count} ${test_ip} > /dev/null; then
       echo "Have internet connectivity"
    else
       echo "Do not have connectivity, rebooting modem internet"
       curl -X 'GET' -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8' -H 'Accept-Encoding: gzip, deflate' -H 'Accept-Language: en-US,en;q=0.9,pt;q=0.8' -H 'Connection: keep-alive' -H 'Referer: http://192.168.1.1/wanst.htm' -H 'Upgrade-Insecure-Requests: 1' -H 'User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36' 'http://192.168.1.1/log/in?un=admin&pw=admin&rd=%2Fuir%2Fstatus.htm&rd2=%2Fuir%2Fwanst.htm&Nrd=1&gws_rd=ssl'
       curl -X 'GET' -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8' -H 'Accept-Encoding: gzip, deflate' -H 'Accept-Language: en-US,en;q=0.9,pt;q=0.8' -H 'Connection: keep-alive' -H 'Cookie: AMSESSIONID=8325491' -H 'Referer: http://192.168.1.1/wanst.htm' -H 'Upgrade-Insecure-Requests: 1' -H 'User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36' 'http://192.168.1.1/uir/rebo.htm?Nrd=0&ZT=1531869136931'
    fi
 }

 check_connectivity
