#!/bin/bash

# Update
apt-get update
apt-get upgrade -y
apt-get dist-upgrade -y
rpi-update

# swap disable
# http://ideaheap.com/2013/07/stopping-sd-card-corruption-on-a-raspberry-pi/
sudo dphys-swapfile swapoff
# sudo dphys-swapfile uninstall
# sudo update-rc.d dphys-swapfile remove

# Kernel panic setup
sed -e '$s/$/\nkernel.panic=3\nkernel.panic_on_oops=3/' -i /etc/sysctl.conf

# crontab
crontab $(pwd)/crontab.bak

# Remot3
apt-get install -y weavedconnectd

# supervisor
apt-get install -y supervisor

# install docker
curl -sSL https://get.docker.com | sh
usermod -aG docker $(USER)

# Pull image
docker login -u retinavision
docker pull retinavision/brain_arm:develop
