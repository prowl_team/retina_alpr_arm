#!/bin/bash

docker run -d \
        --name=brain_arm \
        --restart unless-stopped \
        -p=5000:5000 \
        -p=5555:5555 \
        -p=8080:8080 \
        -p=8081:8081 \
        --device=/dev/vcsm \
        --device=/dev/vchiq --privileged \
        --volume=$(pwd)/supervisor.conf:/etc/supervisor/conf.d/supervisor.conf \
	--volume=$(pwd)/db:/etc/owl_client/owl_client/db \
	--volume=$(pwd)/images:/etc/owl_client/owl_client/files/images \
        --volume=/opt/vc:/opt/vc \
        --volume=/var/log/:/var/log/ \
        retinavision/brain_arm:stag
